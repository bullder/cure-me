require('./bootstrap');

import Search from './components/Search.vue';
import Sources from './components/Sources.vue';
import Relations from './components/Relations.vue';

const app = new Vue({
    el: '#app',
    components: { Search, Sources, Relations }
});
