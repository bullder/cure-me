<?php

use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as BaseResponse;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    const API_PREFIX = 'api/';
    const VERSION = 'v0/';

    /**
     * @param array $params
     * @return string
     */
    public static function prefixURI(array $params): string
    {

        return self::API_PREFIX . self::VERSION . implode('/', $params);
    }

    /**
     * @param BaseResponse $response
     * @param int $code
     */
    public static function isSuccess(BaseResponse $response, $code = Response::HTTP_OK)
    {
        self::assertEquals($code, $response->status());
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }
}
