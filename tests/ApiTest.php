<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;

class ApiTest extends TestCase
{
    use DatabaseMigrations;

    const TARGET = 'source';
    const TARGET_RELATION = 'relation';

    protected function artisanMigrateRefresh()
    {
        Artisan::call('migrate');
    }

    protected function artisanMigrateReset()
    {
        Artisan::call('migrate:reset');
    }

    private function getModel($medrec = '99999', $idc = '27986', $name = 'Magister Yoda')
    {
        return ['medrec' => $medrec, 'icd' => $idc, 'name' => $name];
    }

    private function getNdcModel($medrec = '99999', $ndc = 'NDC10000-0475')
    {
        return ['medrec' => $medrec, 'ndc' => $ndc];
    }

    private function prepareSearch()
    {
        $yoda = $this->getModel();
        $wader = $this->getModel('66666', '66666', 'Darth Wader');
        $maul = $this->getModel('66661', '66661', 'Darth Maul');
        $this->testPost($yoda);
        $this->testPost($wader);
        $this->testPost($maul);

        $rel = [];
        $rel[] = $this->getNdcModel($yoda['medrec']);
        $rel[] = $this->getNdcModel($wader['medrec']);
        $rel[] = $this->getNdcModel($wader['medrec'], 'NDC10000-0999');
        $rel[] = $this->getNdcModel($maul['medrec']);
        $rel[] = $this->getNdcModel($maul['medrec'], 'NDC10000-0999');
        $rel[] = $this->getNdcModel($maul['medrec'], 'NDC10000-1999');

        foreach ($rel as $relation) {
            $this->testPostRelation($relation);
        }
    }

    /**
     * @param null $model
     */
    public function testPost($model = null)
    {
        if (!$model) {
            $model = $this->getModel();
        }

        $response = $this->call('POST', self::prefixURI([self::TARGET]), $model);
        self::isSuccess($response);
    }

    public function testGetCreatedModel()
    {
        $model = $this->getModel();
        $this->testPost();
        $id = 1;
        $response = $this->call('GET', self::prefixURI([self::TARGET, $id]));
        self::isSuccess($response);
        $model['id'] = $id;
        $this->assertEquals($model, (array) json_decode($response->getContent()));
    }

    public function testUpdate()
    {
        $this->testPost();

        $id = 1;
        $model = $this->getModel('66666', '66666', 'Darth Wader');

        $response = $this->call('PUT', self::prefixURI([self::TARGET, $id]), $model);
        self::isSuccess($response);

        $response = $this->call('GET', self::prefixURI([self::TARGET, $id]));
        self::isSuccess($response);
        $model['id'] = $id;
        $this->assertEquals($model, (array) json_decode($response->getContent()));
    }

    public function testDelete()
    {
        $this->testPost();

        $id = 1;
        $response = $this->call('DELETE', self::prefixURI([self::TARGET, $id]));
        self::isSuccess($response);

        $response = $this->call('GET', self::prefixURI([self::TARGET, $id]));
        self::isSuccess($response, Response::HTTP_NOT_FOUND);
    }

    public function testIndex()
    {
        $this->testPost();
        $this->testPost($this->getModel('66666', '66666', 'Darth Wader'));

        $response = $this->call('GET', self::prefixURI([self::TARGET]));
        self::isSuccess($response);
        $arrayResponse = (array) json_decode($response->getContent());
        $this->assertEquals(2, $arrayResponse['total']);
        $this->assertCount(2, $arrayResponse['data']);
    }

    public function testSearchModel()
    {
        $this->testPost();
        $this->testPost($this->getModel('66666', '66666', 'Darth Wader'));

        $response = $this->call('GET', self::prefixURI([self::TARGET]));
        self::isSuccess($response);
        $arrayResponse = (array) json_decode($response->getContent());
        $this->assertEquals(2, $arrayResponse['total']);
        $this->assertCount(2, $arrayResponse['data']);
    }

    /**
     * @param null $model
     */
    public function testPostRelation($model = null)
    {
        if (!$model) {
            $model = $this->getNdcModel();
        }

        $response = $this->call('POST', self::prefixURI([self::TARGET_RELATION]), $model);
        self::isSuccess($response);
    }

    public function testSearch(string $testTarget = null)
    {
        $this->prepareSearch();

        if ($testTarget === null) {
            $testTarget = self::prefixURI(['search']);
        }

        $searchQuery = ['name' => 'Darth', 'count' => 0, 'force' => true];
        $response = $this->call('GET', $testTarget, $searchQuery);
        $arrayResponse = (array) json_decode($response->getContent());
        $this->assertCount(2, $arrayResponse['data']);

        self::isSuccess($response);

        $searchQuery = ['name' => 'Darth', 'count' => 2, 'force' => true];
        $response = $this->call('GET', $testTarget, $searchQuery);
        $arrayResponse = (array) json_decode($response->getContent());
        $this->assertCount(1, $arrayResponse['data']);

        self::isSuccess($response);

        $searchQuery = ['name' => 'Yoda', 'count' => 1, 'force' => true];
        $response = $this->call('GET', $testTarget, $searchQuery);
        $arrayResponse = (array) json_decode($response->getContent());
        $this->assertCount(0, $arrayResponse['data']);

        self::isSuccess($response);
    }

    public function testSearchSQL()
    {
        $this->testSearch(self::prefixURI(['searchSQL']));
    }

    public function testSearchCache(string $testTarget = null)
    {
        if ($testTarget === null) {
            $testTarget = self::prefixURI(['search']);
        }

        $searchQuery = ['name' => 'Darth', 'count' => 0];

        Cache::shouldReceive('get')->once()->andReturn(null);
        Cache::shouldReceive('put')->once();
        $response = $this->call('GET', $testTarget, $searchQuery);
        self::isSuccess($response);

        Cache::shouldReceive('get')->once()->andReturn(['some_cached' => 'cached_body']);
        Cache::shouldReceive('put')->never();
        $response = $this->call('GET', $testTarget, $searchQuery);
        self::isSuccess($response);
    }

    public function testSearchSQLCache()
    {
        $this->testSearchCache(self::prefixURI(['searchSQL']));
    }
}
