<?php
/**
 * Created by PhpStorm.
 * User: bullder
 * Date: 3/25/2017
 * Time: 5:25 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller;

abstract class AbstractApiController extends Controller
{
    /**
     * @var string
     */
    protected $targetEntity;

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->targetEntity::paginate());
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function get(int $id): JsonResponse
    {
        $entity = $this->targetEntity::find($id);
        if ($entity) {
            return response()->json($entity);
        }

        return response()->json($entity, Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = $this->targetEntity::create($request->all());

        return response()->json($entity);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $entity = $this->targetEntity::find($id);
        $entity->delete();

        return response()->json('deleted');
    }
}
