<?php

namespace App\Http\Controllers;

use \DB;
use \Cache;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Laravel\Lumen\Routing\Controller;

class CommonApiController extends Controller
{
    const CACHE_TIME = 10;
    const FORCE_PARAMETER = 'force';
    const PER_PAGE = 15;

    /**
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $cache = $this->tryGetCached($request, __FUNCTION__);
        if ($cache) {
            return response()->json($cache->getData());
        }

        $name = $request->get('name');
        $count = (int) $request->get('count');
        $sources = DB::table('sources')
            ->select(DB::raw('icd, name, count(*) as `ndc_count`'))
            ->join('relations', 'relations.medrec', 'sources.medrec')
            ->groupBy(['icd', 'name']);

        if ($name !== null) {
            $sources->where('name', 'LIKE', '%' . $name . '%');
        }

        if ($count !== null) {
            $sources->havingRaw('COUNT(*) > '. $count);
        }

        $response = $sources->simplePaginate();

        self::cacheResponseForRequest($request, $response, __FUNCTION__);

        return response()->json($response);
    }

    public function searchSQL(Request $request): JsonResponse
    {
        $cache = $this->tryGetCached($request, __FUNCTION__);
        if ($cache) {
            return response()->json($cache->getData());
        }

        $name = $request->get('name');
        $count = (int) $request->get('count');
        $page = (int) $request->get('page', 1);
        if ($page < 1) {
            $page = 1;
        }
        $mainSelect = 'SELECT icd, name, count(*) AS `ndc_count` FROM `sources`';
        $countSelect = 'SELECT count(*) AS `ndc_count` FROM `sources`';
        $sql = ' INNER JOIN `relations` ON `relations`.`medrec` = `sources`.`medrec`';

        $sqlParams = [];
        if ($name !== null) {
            $sql .= ' WHERE `name` LIKE :name';
            $sqlParams['name'] = '%' . $name . '%';
        }

        $sql .= ' GROUP BY `icd`, `name`';

        if ($count > 0) {
            $sql .= ' HAVING `ndc_count` > :count';
            $sqlParams['count'] = $count;
        }

        $total = DB::select(DB::raw($countSelect . $sql), $sqlParams);
        $sql .= ' LIMIT :limit OFFSET :offset';
        $sqlParams['offset'] = ($page - 1) * self::PER_PAGE;
        $sqlParams['limit'] = self::PER_PAGE;

        $sources['next_page_url'] = null;
        $sources['prev_page_url'] = null;
        if ($page > 1) {
            $sources['prev_page_url'] = $page - 1;
        }
        if ($page * self::PER_PAGE < count($total)) {
            $sources['next_page_url'] = $page + 1;
        }
        $sources['data'] = DB::select(DB::raw($mainSelect . $sql), $sqlParams);
        self::cacheResponseForRequest($request, $sources, __FUNCTION__);

        return response()->json($sources);
    }

    private static function prefixCache(Request $request, $key)
    {
        return $key . '_' . md5($request);
    }

    private static function isForcedRequest(Request $request): bool
    {
        return (bool) $request->get(self::FORCE_PARAMETER, false);
    }

    private static function cacheResponseForRequest(Request $request, $object, string $action)
    {
        Cache::put(self::prefixCache($request, $action), $object, self::CACHE_TIME);
    }

    private function tryGetCached($request, $action)
    {
        $cachedResponse = Cache::get(self::prefixCache($request, $action));

        if ($cachedResponse && !self::isForcedRequest($request)) {
            return response()->json($cachedResponse);
        }

        return null;
    }
}
