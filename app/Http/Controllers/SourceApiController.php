<?php
/**
 * Created by PhpStorm.
 * User: bullder
 * Date: 3/25/2017
 * Time: 12:42 AM
 */

namespace App\Http\Controllers;

use App\Source;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SourceApiController extends AbstractApiController implements CrudInterface
{
    /**
     * @var string
     */
    protected $targetEntity = Source::class;

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        /** @var Source $source */
        $source  = $this->targetEntity::find($id);
        $source->medrec = $request->input('medrec');
        $source->icd = $request->input('icd');
        $source->name = $request->input('name');
        $source->save();

        return response()->json($source);
    }
}
