<?php
/**
 * Created by PhpStorm.
 * User: bullder
 * Date: 3/25/2017
 * Time: 5:29 AM
 */

namespace App\Http\Controllers;

use App\Relation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RelationApiController extends AbstractApiController implements CrudInterface
{
    /**
     * @var string
     */
    protected $targetEntity = Relation::class;

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        /** @var Relation $model */
        $model = $this->targetEntity::find($id);
        $model->medrec = $request->input('medrec');
        $model->ndc = $request->input('ndc');
        $model->save();

        return response()->json($model);
    }
}
