<?php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;

class WebController extends Controller
{
    public function index()
    {
        return view('index');
    }
}
