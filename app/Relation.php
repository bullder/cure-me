<?php
/**
 * Created by PhpStorm.
 * User: bullder
 * Date: 3/25/2017
 * Time: 12:40 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Relation
 *
 * @property string $medrec
 * @property string $ndc
 * @property int $id
 * @method static \Illuminate\Database\Query\Builder|\App\Relation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Relation whereMedrec($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Relation whereNdc($value)
 * @mixin \Eloquent
 */
class Relation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id', 'medrec', 'ndc'
    ];
}
