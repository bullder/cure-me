<?php
/**
 * Created by PhpStorm.
 * User: bullder
 * Date: 3/25/2017
 * Time: 12:35 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Source
 *
 * @property string $medrec
 * @property string $icd
 * @property string $name
 * @property int $id
 * @method static \Illuminate\Database\Query\Builder|\App\Source whereIcd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Source whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Source whereMedrec($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Source whereName($value)
 * @mixin \Eloquent
 */
class Source extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id', 'medrec', 'icd', 'name'
    ];
}
