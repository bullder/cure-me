<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BeautifyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (App::environment('testing')) {
            Schema::dropIfExists('relations');
            Schema::create('relations', function (Blueprint $table) {
                $table->increments('id');
                $table->char('medrec', 10);
                $table->char('ndc', 20);
                $table->index('medrec', 'relations_medrec_idx');
            });
            Schema::dropIfExists('sources');
            Schema::create('sources', function (Blueprint $table) {
                $table->increments('id');
                $table->char('medrec', 10);
                $table->char('icd', 10);
                $table->char('name', 100);
                $table->index('medrec', 'sources_medrec_idx');
            });

            return;
        }

        Schema::rename('tb_rel', 'relations');
        Schema::table('relations', function (Blueprint $table) {
            $table->increments('id');
            $table->renameColumn('MEDREC_ID', 'medrec');
            $table->renameColumn('NDC', 'ndc');
            $table->index('medrec', 'relations_medrec_idx');
        });

        Schema::rename('tb_source', 'sources');
        Schema::table('sources', function (Blueprint $table) {
            $table->increments('id');
            $table->renameColumn('MEDREC_ID', 'medrec');
            $table->renameColumn('ICD', 'icd');
            $table->renameColumn('PATIENT_NAME', 'name');
            $table->index('medrec', 'sources_medrec_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (App::environment('testing')) {
            Schema::dropIfExists('relations');
            Schema::dropIfExists('sources');

            return;
        }

        Schema::table('relations', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->renameColumn('medrec', 'MEDREC_ID');
            $table->renameColumn('ndc', 'NDC');
            $table->dropIndex('relations_medrec_idx');
        });
        Schema::table('sources', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->renameColumn('medrec', 'MEDREC_ID');
            $table->renameColumn('icd', 'ICD');
            $table->renameColumn('name', 'PATIENT_NAME');
            $table->dropIndex('sources_medrec_idx');
        });
        Schema::rename('relations', 'tb_rel');
        Schema::rename('sources', 'tb_source');
    }
}
