<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Migrations\Migration;

class LoadDump extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!App::environment('testing')) {
            foreach (Storage::disk()->files('dump/') as $file) {
                echo 'Dumping ' . $file . PHP_EOL;
                DB::unprepared(Storage::disk()->get($file));
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!App::environment('testing')) {
            Schema::dropIfExists('tb_rel');
            Schema::dropIfExists('tb_source');
        }
    }
}
