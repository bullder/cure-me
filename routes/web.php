<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'WebController@index');

$app->group(['prefix' => 'api/v0'], function($app)
{
    $app->get('relation', 'RelationApiController@index');
    $app->get('relation/{id}', 'RelationApiController@get');
    $app->post('relation', 'RelationApiController@create');
    $app->put('relation/{id}', 'RelationApiController@update');
    $app->delete('relation/{id}', 'RelationApiController@delete');

    $app->get('source', 'SourceApiController@index');
    $app->get('source/{id}', 'SourceApiController@get');
    $app->post('source', 'SourceApiController@create');
    $app->put('source/{id}', 'SourceApiController@update');
    $app->delete('source/{id}', 'SourceApiController@delete');

    $app->get('search', 'CommonApiController@search');
    $app->get('searchSQL', 'CommonApiController@searchSQL');
});
