#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0
set -xe

apt-get update -yqq
apt-get install git libcurl4-gnutls-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev memcached -yqq

docker-php-ext-install mbstring pdo pdo_mysql curl json intl gd zip bz2 opcache

pecl install xdebug

docker-php-ext-enable xdebug

curl -sS https://getcomposer.org/installer | php
php composer.phar install --prefer-dist > /dev/null

touch database/sqlite/test.sqlite
cp ./.env.testing.example ./.env.testing

#php arisan migrate --env=testing